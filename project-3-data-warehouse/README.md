# Project: Data Warehouse

## Introduction
A music streaming startup, Sparkify, has grown their user base and song database and want to move their processes and 
data onto the cloud. Their data resides in S3, in a directory of JSON logs on user activity on the app, as well as a 
directory with JSON metadata on the songs in their app.

As their data engineer, you are tasked with building an ETL pipeline that extracts their data from S3, stages them in 
Redshift, and transforms data into a set of dimensional tables for their analytics team to continue finding insights in
what songs their users are listening to. You'll be able to test your database and ETL pipeline by running queries given 
to you by the analytics team from Sparkify and compare your results with their expected results.

## Project structure
| File   | Description      | 
|--------|------------------|
| song data | a folder available in s3://udacity-dend/song_data containing the songs data set |
| log_data | a folder available in s3://udacity-dend/log_data containing the data describing users' activity |
| create_table.py | SQL scripts to create and delete, the staging, the fact and dimension tables for the star schema in Redshift |
| etl.py | loads data from S3 into staging tables on Redshift and then processes that data into the analytics tables on Redshift |
| sql_queries.py | defines the SQL query statements, which will be imported into the two other files above |

## Executing the project

```
Prerequisites:
In the dwh.cfg file, set the HOST, DB_NAME, DB_USER, DB_PASSWORD and the ARN entries according to the details of your Redshift cluster and the IAM role that will be used when running the scripts.
````

1. Run ```python create_tables.py``` 
2. Run ```python etl.py```

## Data model

### Model
The star schema for Sparkify's analytics
![Sparkify Model](sparkify_model.png)

### Model description
The fact table songplays contains the events that represent user activities in the app regarding songs streaming.

Dimension tables users, time, songs and artists expand the data in the fact table in order to further better categorize the events.

## Analytics goals
The model allows measuring songs reproductions, obtaining immediate insights about the location where those songs 
(and the artists) are being listened from, the kind of devices used to stream music, the behavior of paid vs free accounts,
among other. The analysis can be potentially used to recommend songs and artists to users based on their musical taste,
inform a user about local events that include their favorite artists, optimize the content and user experience for specific
devices, etc.

## Specific decisions about the data model and the queries
1. All dimension tables contain a primary key and the fact table references them accordingly, although it seems Redshift 
   doesn't enforce them.
2. Only the last subscription level of a user is stored in the Users table, this is achieved by finding the last record 
   of a specific user in the events' staging table.
   The staging_events.ts attribute was marked as sort key to allow this in a performant way.
3. The INSERT statement for the artists table could allow duplicates, but since the current data doesn't have any
   duplicated artists, I decided not to overcomplicate the INSERT.
   
## ETL

### The ETL process

#### Step 1
The JSON files containing the songs and logs data are store in staging tables in Redshift using the ```copy``` command.

#### Step 2
The analytics tables are filled from the content of the staging tables using SQL INSERT statements.
