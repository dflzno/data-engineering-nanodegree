# Udacity's Data Engineering nanodegree

## Postgres

### Pull Postgres
```
docker pull postgres
```
### Run Postgres
```
docker run --rm --name c-postgres -e POSTGRES_USER=student -e POSTGRES_PASSWORD=student -e POSTGRES_DB=studentdb -d -p 5432:5432 postgres
```

## Jupyter

### Pull Jupyter minimal-notebook
```
docker pull jupyter/minimal-notebook
```
### Run Jupyter
```
docker run -it --rm --name c-jupyter-minimal -p 8888:8888 -v [host_folder_with_notebooks]:/home/jovyan jupyter/minimal-notebook
```
